package com.example.covid_base.repositories;

import com.example.covid_base.entities.PatientsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientsRepository extends JpaRepository<PatientsEntity,Integer> {
}
