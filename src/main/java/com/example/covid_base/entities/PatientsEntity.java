package com.example.covid_base.entities;

import javax.persistence.*;

@Entity
@Table(name = "patients")
public class PatientsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_Name")
    private String firstName;

    @Column(name = "last_Name")
    private String lastName;

    @Column(name = "age")
    private int age;

    @Column(name = "vaccination")
    private boolean vaccination;

    @Column(name = "got_infected")
    private boolean got_infected;


    public PatientsEntity() {
    }

    public PatientsEntity(int id,String firstName, String lastName, int age, boolean vaccination, boolean got_infected) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.vaccination = vaccination;
        this.got_infected = got_infected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isVaccination() {
        return vaccination;
    }

    public void setVaccination(boolean vaccination) {
        this.vaccination = vaccination;
    }

    public boolean isGot_infected() {
        return got_infected;
    }

    public void setGot_infected(boolean got_infected) {
        this.got_infected = got_infected;
    }

    @Override
    public String toString() {
        return "PatientsEntity{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", vaccination=" + vaccination +
                ", got_infected=" + got_infected +
                '}';
    }
}
