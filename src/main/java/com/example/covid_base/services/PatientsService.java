package com.example.covid_base.services;

import com.example.covid_base.entities.PatientsEntity;

public interface PatientsService {
    public String getAllPatients();
    public String getById(int id);
    public String addPatient(PatientsEntity patientsEntity);
    public String updatePatient(PatientsEntity patientsEntity);
    public String deleteById(int id);
}
