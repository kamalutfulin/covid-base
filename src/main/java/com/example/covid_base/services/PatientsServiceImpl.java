package com.example.covid_base.services;

import com.example.covid_base.entities.PatientsEntity;
import com.example.covid_base.repositories.PatientsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

@Service
public class PatientsServiceImpl implements PatientsService {
    @Autowired
    PatientsRepository patientsRepository;

    @Override
    public String getAllPatients() {
        return patientsRepository.findAll().toString();
    }

    @Override
    public String getById(int id) {
        return patientsRepository.getById(id).toString();
    }

    @Override
    public String addPatient(PatientsEntity patientsEntity){
        patientsRepository.save(patientsEntity);
        return "Объект: " + patientsEntity.toString() + " Cохранен";
    }

    public String updatePatient( PatientsEntity patientsEntity) {
        if (patientsEntity.getAge() == 0 || patientsEntity.getFirstName() == null || patientsEntity.getLastName() == null ){
            return "Заполните полностью";
        }

        patientsRepository.save(patientsEntity);

        return "Пациент изменен";
    }

    @Override
    public String deleteById(int id){
        patientsRepository.deleteById(id);
        return "Объект с id " + id + " удалён";
    }
}
