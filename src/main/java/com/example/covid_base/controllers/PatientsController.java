package com.example.covid_base.controllers;

import com.example.covid_base.entities.PatientsEntity;
import com.example.covid_base.services.PatientsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patients")
@Api(value = "patients", description = "Контроллер для операций с пациентом")


public class PatientsController {
    @Autowired
    PatientsService patientsService;

    @GetMapping("")
    @ApiOperation("Получить весь список пациентов")
    public String getAllPatients() {
        return patientsService.getAllPatients();
    }

    @GetMapping("/{id}")
    @ApiOperation("Получить пациента по id")
    public String getById(@PathVariable int id) {
        return patientsService.getById(id);
    }

    @PostMapping("")
    @ApiOperation("Создать пациента")
    public String addPatient(@RequestBody PatientsEntity patientsEntity) {
        return patientsService.addPatient(patientsEntity);
    }
    @PutMapping("")
    @ApiOperation("Изменить данные пациента")
    public String updatePatient(@RequestBody PatientsEntity patientsEntity) {
        return patientsService.updatePatient(patientsEntity);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удалить пациента")
    public String deleteById(@PathVariable int id) {
        return patientsService.deleteById(id);
    }
}
