Covid-Base - база данных пациентов ковидного центра.
Swagger документация проекта - http://127.0.0.1:8080/swagger-ui/
Реализованы CRUD операции:
GET /patients/{id} Получить пациента по id
GET /patients Получить весь список пациентов
POST /patients Создать пациента
PUT /patients Изменить данные пациента
DELETE /patients/{id} Удалить пациента